<?php

require_once __DIR__ . '/../../../maintenance/Maintenance.php';
class FixFandomProtections extends Maintenance {

	public function __construct() {
		parent::__construct();
		$this->addOption( 'dry', 'Do a dry run and output the changes to terminal' );
	}


	public function execute()
	{
		$db = $this->getDB(DB_PRIMARY);


		$queryBuilder = $db->newSelectQueryBuilder()
			->select(['log_params', 'log_page'])
			->from('logging')
			->where([
				'log_action' => 'protect'
			]);

		$res = $queryBuilder->fetchResultSet();

		if ( $res->numRows() > 0 && !$this->hasOption( 'dry' ) ) {

			foreach ($res as $row) {
				$logParams = unserialize( $row->log_params );
//				$this->output( print_r($logParams['details'], true) );
//
				$edit = $logParams['details'];
				$page = $row->log_page;
//
//				$logParams = unserialize($row->log_params);
//				$expiry = [];
//				$protections = [];
//				if (isset($logParams['details']) && is_array($logParams['details'])) {
//					foreach ($logParams['details'] as $detail) {
//						$type = $detail['type'];
//						$expiry[$type] = $detail['expiry'];
//					}
//				} else {
//					throw new LogicException('There was an error; the data is not an array');
//				}
//
				$wikiPage = \MediaWiki\MediaWikiServices::getInstance()->getWikiPageFactory();

				$pageObject = $wikiPage->newFromID($page);
				$cascade = false;
				$expiry['move'] = 'indefinite';
				$expiry['edit'] = 'indefinite';
				try {
					// re-protect the page with the data we got from the previous database entries
					$pageObject->doUpdateRestrictions($edit, $expiry, $cascade, "Database fix",
						User::newSystemUser('Maintenance'));
				} catch ( Exception $e) {
					$this->output("Exception caught: " . $e->getMessage() );
				}
			}
		} else {
			$this->output('no results');
		}

		return true;
	}
}

$maintClass = FixFandomProtections::class;
require_once RUN_MAINTENANCE_IF_MAIN;
