<?php
/**
 * Generate Pagination for Special Pages or otherwise
 *
 * @package TelepediaMagic
 * @author Noah Manneschmidt
 * @link https://gitlab.com/hydrawiki/extensions/hydracore/-/blob/master/classes/HydraCore.php
 */
namespace Telepedia\TelepediaMagic;

use Title;

class Pagination {

	/**
	 * Generates page numbers.
	 * Call this function directly if a custom pagination template is required otherwise use generatePaginationHtml().
	 *
	 * @access	public
	 * @param	integer	Total number of items to be paginated.
	 * @param	integer	How many items to display per page.
	 * @param	integer	Start Position
	 * @param	integer	Number of extra page numbers to show.
	 * @return	array	Generated array of pagination information.
	 */
	static public function generatePagination($totalItems, $itemsPerPage = 100, $itemStart = 0, $extraPages = 4) {
		if ($totalItems < 1) {
			return false;
		}

		$currentPage	= floor($itemStart / $itemsPerPage) + 1;
		$totalPages		= ceil($totalItems / $itemsPerPage);
		$lastStart		= floor($totalItems / $itemsPerPage) * $itemsPerPage;

		$pagination['first']	= ['st' => 0, 'selected' => false];
		$pagination['last']		= ['st' => $lastStart, 'selected' => false];
		$pagination['stats']	= [
			'pages' => $totalPages,
			'total' => $totalItems,
			'current_page' => $currentPage,
			'items_start' => $itemStart + 1,
			'items_end' => $itemStart + ($itemsPerPage - ($currentPage * $itemsPerPage - min($currentPage * $itemsPerPage, $totalItems)))
		];

		$pageStart	= min($currentPage, $currentPage - ($extraPages / 2));
		$pageEnd	= min($totalPages, $currentPage + ($extraPages / 2));

		if ($pageStart <= 1) {
			$pageStart = 1;
			$pageEnd = $pageStart + $extraPages;
		}
		if ($pageEnd >= $totalPages) {
			$pageEnd = $totalPages;
			$pageStart = max($pageEnd - $extraPages, ($currentPage - ($extraPages / 2)) - (($extraPages / 2) - ($pageEnd - $currentPage)));
		}

		for ($i = $pageStart; $i <= $pageEnd; $i++) {
			if ($i > 0) {
				$pagination['pages'][$i] = ['st' => ($i * $itemsPerPage) - $itemsPerPage, 'selected' => ($i == $currentPage ? true : false)];
			}
		}

		return $pagination;
	}

	static public function generatePaginationHtml(Title $title, int $totalItems, int
	$itemsPerPage = 50, int $itemStart = 0, int $extraPages = 4, array $extraArguments = [], bool
	$showTotal = true) {
		$pagination = self::generatePagination($totalItems, $itemsPerPage, $itemStart, $extraPages);
		$pagination['extra'] = $extraArguments;
		$pagination['showTotal'] = false;
		$templates = new TemplatePagination;
		$html = $templates->pagination($pagination, $title);

		return $html;
	}



}