<?php

namespace Telepedia\TelepediaMagic;

use Html;
use HtmlArmor;
use MediaWiki\Cache\Hook\MessageCacheFetchOverridesHook;
use MediaWiki\Linker\Hook\HtmlPageLinkRendererEndHook;
use MediaWiki\MainConfigNames;
use MediaWiki\MediaWikiServices;
use MessageCache;
use Miraheze\CreateWiki\Hooks\CreateWikiDeletionHook;
use Miraheze\ManageWiki\Helpers\ManageWikiSettings;
use Skin;
use SpecialPage;
use Title;
use User;
use Wikimedia\IPUtils;

class Hooks implements MessageCacheFetchOverridesHook, HtmlPageLinkRendererEndHook {

    public function onMessageCacheFetchOverrides(array &$keys): void {
        static $keysToOverride = [
            'copyrightwarning',
            'pagetitle',
            'group-staff-member',
            'group-steward-member',
            'group-saber-member',
            'vector-night-mode-issue-reporting-notice-url'
        ];

        $languageCode = MediaWikiServices::getInstance()->getMainConfig()->get(MainConfigNames::LanguageCode);

        $transformationCallback = static function (string $key, MessageCache $cache) use ($languageCode): string {
            $transformedKey = "telepedia-$key";

            // MessageCache uses ucfirst if ord( key ) is < 128, which is true of all
            // of the above.  Revisit if non-ASCII keys are used.
            $ucKey = ucfirst($key);

            if (
                /*
                 * Override order:
                 * 1. If the MediaWiki:$ucKey page exists, use the key unprefixed
                 * (in all languages) with normal fallback order.  Specific
                 * language pages (MediaWiki:$ucKey/xy) are not checked when
                 * deciding which key to use, but are still used if applicable
                 * after the key is decided.
                 *
                 * 2. Otherwise, use the prefixed key with normal fallback order
                 * (including MediaWiki pages if they exist).
                 */
                $cache->getMsgFromNamespace($ucKey, $languageCode) === false
            ) {
                return $transformedKey;
            }

            return $key;
        };

        foreach ($keysToOverride as $key) {
            $keys[$key] = $transformationCallback;
        }
    }

    /**
     * Enables global interwiki for [[tp:wiki:Page]]
     */
    public function onHtmlPageLinkRendererEnd($linkRenderer, $target, $isKnown, &$text, &$attribs, &$ret) {
        $target = (string)$target;
        $tooltip = $target;
        $useText = true;

        $ltarget = strtolower($target);
        $ltext = strtolower(HtmlArmor::getHtml($text));

        if ($ltarget == $ltext) {
            // Allow link piping, but don't modify $text yet
            $useText = false;
        }

        $target = explode(':', $target);

        if (count($target) < 2) {
            // Not enough parameters for interwiki
            return true;
        }

        if ($target[0] == '0') {
            array_shift($target);
        }

        $prefix = strtolower($target[0]);

        if ($prefix != 'tp') {
            // Not interesting
            return true;
        }

        $wiki = strtolower($target[1]);
        $target = array_slice($target, 2);
        $target = implode(':', $target);

        if (!$useText) {
            $text = $target;
        }
        if ($text == '') {
            $text = $wiki;
        }

        $target = str_replace(' ', '_', $target);
        $target = urlencode($target);
        $linkURL = "https://$wiki.telepedia.net/wiki/$target";

        $attribs = [
            'href' => $linkURL,
            'class' => 'extiw',
            'title' => $tooltip
        ];

        return true;
    }

    /**
     * Hard redirects all pages like Tp:Wiki:Page as global interwiki.
     */
    public static function onInitializeArticleMaybeRedirect($title, $request, &$ignoreRedirect, &$target, $article) {
        $title = explode(':', $title);
        $prefix = strtolower($title[0]);

        if (count($title) < 3 || $prefix !== 'tp') {
            return true;
        }

        $wiki = strtolower($title[1]);
        $page = implode(':', array_slice($title, 2));
        $page = str_replace(' ', '_', $page);
        $page = urlencode($page);

        $target = "https://$wiki.telepedia.net/wiki/$page";

        return true;
    }

    public static function onTitleReadWhitelist(Title $title, User $user, &$whitelisted) {
        if ($title->equals(Title::newMainPage())) {
            $whitelisted = true;
            return;
        }

        $specialsArray = [
            'CentralAutoLogin',
            'CentralLogin',
            'ConfirmEmail',
            'CreateAccount',
            'Notifications',
            'OAuth',
            'ResetPassword',
            'Watchlist'
        ];

        if ($title->isSpecialPage()) {
            $rootName = strtok($title->getText(), '/');
            $rootTitle = Title::makeTitle($title->getNamespace(), $rootName);

            foreach ($specialsArray as $page) {
                if ($rootTitle->equals(SpecialPage::getTitleFor($page))) {
                    $whitelisted = true;
                    return;
                }
            }
        }
    }

    public static function onGlobalUserPageWikis(&$list) {
        $config = MediaWikiServices::getInstance()->getConfigFactory()->makeConfig('telepediamagic');
        $cwCacheDir = $config->get('CreateWikiCacheDirectory');
        if (file_exists("{$cwCacheDir}/databases.json")) {
            $databasesArray = json_decode(file_get_contents("{$cwCacheDir}/databases.json"), true);
            $list = array_keys($databasesArray['combi']);
            return false;
        }

        return true;
    }

    public static function onSkinAddFooterLinks(Skin $skin, string $key, array &$footerlinks) {
        if ($key === 'places') {
            $footerlinks['discord'] = Html::rawElement('a', ['href' => 'https://discord.com/invite/nenx4zx5uu'], 'Discord');
        }
    }

    public function onCreateWikiDeletion($cwdb, $wiki): void {

        $dbw = MediaWikiServices::getInstance()->getDBLoadBalancerFactory()
            ->getMainLB($this->options->get('EchoSharedTrackingDB'))
            ->getMaintenanceConnectionRef(DB_PRIMARY, [], $this->options->get('EchoSharedTrackingDB'));

        $dbw->delete('echo_unread_wikis', ['euw_wiki' => $wiki]);

        foreach ($this->options->get('LocalDatabases') as $db) {
            $manageWikiSettings = new ManageWikiSettings($db);

            foreach ($this->options->get('ManageWikiSettings') as $var => $setConfig) {
                if (
                    $setConfig['type'] === 'database' &&
                    $manageWikiSettings->list($var) === $wiki
                ) {
                    $manageWikiSettings->remove($var);
                    $manageWikiSettings->commit();
                }
            }
        }
    }

    public static function onContributionsToolLinks($id, Title $title, array &$tools, SpecialPage $specialPage) {
        $username = $title->getText();
        if ($specialPage->getUser()->isAllowed('centralauth-lock') && !IPUtils::isIPAddress($username)) {
            $tools['centralauth'] = $specialPage->getLinkRenderer()->makeKnownLink(
                SpecialPage::getTitleFor('CentralAuth', $username),
                strtolower($specialPage->msg('centralauth')->text())
            );
        }
    }
}
