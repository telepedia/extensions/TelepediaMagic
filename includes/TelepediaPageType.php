<?php

namespace Telepedia\TelepediaMagic;

use MediaWiki\MediaWikiServices;
use MediaWiki\Title\Title;

/**
 * TelepediaPageType
 * @desc General utility function to return the type of a page.
 * @file
 * @ingroup Extensions
 * @version 1.1.0
 * @date 2023-12-20
 *
 * @license MIT
 */
class TelepediaPageType {

	/*
	 * Get the current title of the page ($wgTitle)
	 * We also follow redirects for the purpose of being thorough
	 * @return Title|null title instance
	 */
	private static function getTitle() {
		global $wgTitle;
		$title = $wgTitle;

		// follow redirects
		if ($title instanceof Title && $title->isRedirect()) {
			$page = MediaWikiServices::getInstance()->getWikiPageFactory()
				->newFromTitle($title);
			$tmpTitle = $page->getRedirectTarget();

			if ($tmpTitle instanceof Title) {
				$title = $tmpTitle;
			}
		}

		return $title;
	}

	// get the type of page as a string
	public static function getPageType() {
		if (self::isMainPage()) {
			$type = 'home';
		} elseif (self::isFilePage()) {
			$type = 'file';
		} elseif (self::isSearch()) {
			$type = 'search';
		} elseif (self::isSpecial()) {
			$type = 'special';
		} else {
			$type = 'article';
		}

		return $type;
	}


	/**
	 * Check if the current page is the main page
	 * And that we're not doing an action (history, edit)
	 * @return bool
	 */
	public static function isMainPage() {
		$title = self::getTitle();

		$isMainPage = ($title instanceof Title && $title->isMainPage() && !self::isActionPage());

		return $isMainPage;
	}

	/**
	 * Check if current page is file page
	 * If so, we are in the file namespace
	 * @return bool
	 */
	public static function isFilePage() {
		$title = self::getTitle();

		return ($title instanceof Title) && NS_FILE === $title->getNamespace();
	}

	/**
	 * Check if current page is a special page (Special:*)
	 *
	 * @return bool
	 */
	public static function isSpecial() {
		$title = self::getTitle();

		return $title->isSpecialPage();
	}

	public static function isActionPage() {
		global $wgRequest;

		return (
			$wgRequest->getVal('action', 'view') !== 'view' || $wgRequest->getVal('diff') !== null
		);
	}

	/**
	 * Check if current page is Special:Search
	 *
	 * @return bool
	 */
	public static function isSearch() {
		$title = self::getTitle();

		$searchPageNames = ['Search'];

		// Get an instance of SpecialPageFactory via MediaWikiServices
		$specialPageFactory = MediaWikiServices::getInstance()->getSpecialPageFactory();

		// Use the instance to call the non-static method
		$pageNames = $specialPageFactory->resolveAlias($title->getDBkey());

		return ($title instanceof Title) && $title->isSpecialPage()
			&& in_array(array_shift($pageNames), $searchPageNames);
	}
}
